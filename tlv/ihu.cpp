#include "ihu.h"
#include "kernel.h"

#include <cstdlib>
#include <cstring>
#include <iostream>


IHeardYou::IHeardYou(uint8_t * &data, uint16_t length) : TLV(IHEARDYOU_T)
{
    data += BYTE; // skipping Type
    if(length < 2) {
        std::cerr << "Malformed IHeardYou (length(" << length
            << ") < 2)" << std::endl;
        throw DeserializationError();
    } else {
        uint8_t id_length = data[0];
        data += BYTE; // skipping Length
        if(id_length < 8) {
            std::cerr << "Malformed IHeardYou (id_length(" << id_length
                << ") < 8)" << std::endl;
            data += id_length; // skipping rest of TLV
            throw DeserializationError();
        } else {
            this->id = be64toh(*(uint64_t*)data); // copying id
            data += id_length; // skipping rest of TLV
        }
    }
}

void IHeardYou::serialize(uint8_t * &data, uint16_t &length)
{
    length = 2 * BYTE + sizeof(uint64_t);
    data = (uint8_t*)malloc(length);
    data[0] = this->type;
    data[1] = length;
    *(uint64_t*)(data + 2) = htobe64(this->id);
}

void IHeardYou::print(std::ostream &os) const
{
    os << "IHeardYou Id(0x" << std::hex << id << std::dec << ")" << std::endl;
}
