#pragma once

#include <cstdint>
#include <exception>
#include <ostream>

#define BYTE sizeof(uint8_t)

#define PAD1_T            0
#define PADN_T            1
#define IHEARDYOU_T       2
#define NEIGHBORREQUEST_T 3
#define NEIGHBORS_T       4
#define DATATLV_T         5
#define IHAVE_T           6

// Exception thrown when a TLV cannot be deserialized
class DeserializationError : public std::exception
{
    public:
        DeserializationError() {}
};

class TLV
{
    protected:
        uint8_t type;
    public:
        TLV(uint8_t type) : type(type) {}
        virtual ~TLV() {}
        virtual void serialize(uint8_t * &data, uint16_t &length) = 0;
        uint8_t get_type() const { return type; }
        virtual void print(std::ostream &os) const = 0;
};

TLV * deserialize(uint8_t * &data, uint16_t length);
