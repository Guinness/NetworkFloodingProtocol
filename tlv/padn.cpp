#include "padn.h"

#include <cstdlib>
#include <cstring>
#include <iostream>

PadN::PadN(uint8_t * &data, uint16_t length) : TLV(PADN_T)
{
    data += BYTE; // skipping Type
    if(length < 2) {
        std::cerr << "Malformed PadN (length(" << length << ") < 2)"
            << std::endl;
#ifdef __RESILIENT__
        // If the length is not present, the padn can be considered of length
        // 0 even though the specification states it is invalid
        this->length = 0;
#else
        throw DeserializationError();
#endif
    } else {
        // Not checking if MBZ is indeed all zero
        this->length = data[0];
        data += BYTE; // skipping Length
        data += this->length; // skipping MBZ
    }
}

void PadN::serialize(uint8_t * &data, uint16_t &l)
{
    //length * mbz + type + length
    l = (this->length + 2) * BYTE;
    data = (uint8_t*)malloc(l * BYTE);
    memcpy(data, (void*)&this->type, BYTE);
    memcpy(data + BYTE, (void*)&l, BYTE);
    memset(data + 2 * BYTE, 0, this->length);
}

void PadN::print(std::ostream &os) const
{
    os << "Pad1" << std::endl;
}
