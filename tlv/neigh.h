#pragma once

#include "tlv.h"
#include "../peer.h"

#include <cstdint>
#include <list>
#include <memory>

template<typename T>
using list_ptr = std::shared_ptr<std::list<T>>;

struct peeraddr
{
    uint64_t id;
    uint8_t address[16];
    uint16_t port;
    peeraddr(peer &p);
};

class Neighbors : public TLV
{
    private:
        list_ptr<peer*> neighbors;
    public:
        Neighbors(list_ptr<peer*> neighbors)
            : TLV(NEIGHBORS_T), neighbors(neighbors) {}
        // deserialization constructor
        Neighbors(uint8_t * &data, uint16_t length);
        void serialize(uint8_t * &data, uint16_t &length) override;
        void print(std::ostream &os) const override;
};
