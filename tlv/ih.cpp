#include "ih.h"
#include "kernel.h"

#include <arpa/inet.h>
#include <cstdlib>
#include <cstring>
#include <iostream>



IHave::IHave(uint8_t * &data, uint16_t length) : TLV(IHAVE_T)
{
    if(length < 2) {
        std::cerr << "Malformed IHave (length(" << length << ") < 2)"
            << std::endl;
        throw DeserializationError();
    } else {
        uint8_t content_length = data[1];
        uint8_t header_length = sizeof(seqno) + sizeof(id);
        if(content_length < header_length) {
            std::cerr << "Malformed IHave (content_length("
                << content_length << ") < header_length("
                << header_length << ")" << std::endl;
            throw DeserializationError();
        }
        data += 2 * BYTE; // skipping header
        memcpy(&this->seqno, data, sizeof(seqno));
        data += sizeof(seqno); // skipping seqno
        memcpy(&this->id, data, sizeof(id));
        data += sizeof(id); // skipping id
    }
}

void IHave::serialize(uint8_t * &odata, uint16_t &length)
{
    length = 2 * BYTE + sizeof(seqno) + sizeof(id);
    odata = (uint8_t*)malloc(length);
    uint8_t * ptr = odata;
    ptr[0] = this->type;
    ptr += BYTE;
    ptr[0] = length;
    ptr += BYTE;
    *(uint32_t*)ptr = htonl(this->seqno);
    ptr += sizeof(seqno);
    *(uint64_t*)ptr = htobe64(this->id);
}

void IHave::print(std::ostream &os) const
{
    os << "IHave seqno=" << seqno << " id=" << id << std::endl;
}
