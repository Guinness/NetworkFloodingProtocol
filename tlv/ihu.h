#pragma once

#include "tlv.h"

class IHeardYou : public TLV
{
    private:
        uint64_t id;
    public:
        IHeardYou(uint64_t id) : TLV(IHEARDYOU_T), id(id) {}
        IHeardYou(uint8_t * &data, uint16_t length); // deserializing constructor
        void serialize(uint8_t * &data, uint16_t &length) override;
        void print(std::ostream &os) const override;
};
