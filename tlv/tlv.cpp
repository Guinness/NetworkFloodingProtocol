/*
 * This file implements the deserialize function of tlv.h, for deserializing
 * TLVs
 *
 * This function is pretty long, considering moving each case to their
 * respective class.
 */

#include "../peer.h"
#include "headers.h"

#include <cstring>
#include <list>
#include <iostream>

TLV * deserialize(uint8_t * &data, uint16_t length)
{
    if(length == 0) {
        std::cerr << "TLV deserialization: no data" << std::endl;
        return (TLV*)NULL;
    }
    try
    {
        switch(data[0])
        {
            /* -------------------- */
            case PAD1_T:
                return new Pad1();
                break;
                /* -------------------- */
            case PADN_T:
                return new PadN(data, length);
                break;
                /* -------------------- */
            case IHEARDYOU_T:
                return new IHeardYou(data, length);
                break;
                /* -------------------- */
            case NEIGHBORREQUEST_T:
                return new NeighborRequest(data, length);
                break;
                /* -------------------- */
            case NEIGHBORS_T:
                return new Neighbors(data, length);
                break;
                /* -------------------- */
            case DATATLV_T:
                return new DataTLV(data, length);
                break;
                /* -------------------- */
            case IHAVE_T:
                return new IHave(data, length);
                break;
                /* -------------------- */
            default:
                std::cerr << "Unknown TLV (" << +data[0] << ")" << std::endl;
                return (TLV*)NULL;
                break;
        }
    }
    catch(const DeserializationError & e)
    {
        return (TLV*)NULL;
    }
    // Should not happen because of the default case but g++ issues a warning
    // when nothing is returned here
    return (TLV*)NULL;
}
