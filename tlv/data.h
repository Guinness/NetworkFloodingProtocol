#pragma once

#include "tlv.h"

#include <cstdint>
#include <memory>

typedef std::shared_ptr<uint8_t> data_ptr;

class DataTLV : public TLV
{
    private:
        uint32_t seqno;
        uint64_t id;
        data_ptr data;
        uint8_t data_length;
    public:
        DataTLV(uint32_t seqno, uint64_t id,
                data_ptr data, uint16_t data_length)
            : TLV(DATATLV_T), seqno(seqno), id(id),
            data(data), data_length(data_length) {}
        DataTLV(uint8_t * &data, uint16_t length); // deserializing constructor
        void serialize(uint8_t * &data, uint16_t &length) override;
        void print(std::ostream &os) const override;
        uint64_t get_id() const { return id; }
        uint32_t get_seqno() const { return seqno; }
        data_ptr get_data() const { return data; }
        uint8_t get_data_length() const { return data_length; }
};
