#pragma once

#include "tlv.h"
#include <stdint.h>

class Pad1 : public TLV
{
    public:
        Pad1() : TLV(PAD1_T) {}
        void serialize(uint8_t * &data, uint16_t &length) override;
        void print(std::ostream &os) const override;
};
