#include "neigh.h"
#include "kernel.h"

#include <arpa/inet.h>
#include <cstdlib>
#include <cstring>
#include <iostream>

#define PEER_DATA_SIZE 26

peeraddr::peeraddr(peer &p) : id(p.id),
    port(p.address->sin6_port)
{
    memcpy(&this->address, p.address->sin6_addr.s6_addr, 16);
}

Neighbors::Neighbors(uint8_t * &data, uint16_t length)
    : TLV(NEIGHBORS_T), neighbors(std::make_shared<std::list<peer*>>())
{
    data += BYTE; // skipping Type
    if(length < 2) {
        std::cerr << "Malformed Neighbors (length(" << length
            << ") < 2)" << std::endl;
#ifdef __RESILIENT__
        // If the length is not present, the neighbor list can be
        // considered as an empty neighbor list even though the
        // specification states it is invalid
#else
        throw DeserializationError();
#endif
    } else {
        uint8_t data_length = data[0];
        data += BYTE; // skipping Length
        if(data_length % PEER_DATA_SIZE != 0) {
            std::cerr << "Malformed Neighbors (length(" << +data_length
                << ") not a multiple of " << PEER_DATA_SIZE
                << ")" << std::endl;
#ifdef __RESILIENT__
            // If compiling with __RESILIENT__, the extra data is
            // simply discarded
#else
            // If the length of the contents is not a multiple of the
            // size of a peer data structure, reject it.
            throw DeserializationError();
#endif
        }
        uint8_t neighbor_count = data_length / sizeof(peer);
        // interpreting data as a peer table
        peer * raw_peer_data = (peer*)data;
        for(uint8_t i = 0; i < neighbor_count; i++)
        {
            peer * peer_data = &raw_peer_data[i];
            // add the peer to the list
            this->neighbors->push_back(peer_data);
        }
        data += data_length; // skipping all the data
    }
}

void Neighbors::serialize(uint8_t * &data, uint16_t &length)
{
    uint64_t peer_count = neighbors->size();
    length = 2 * BYTE + peer_count * sizeof(peer);
    data = (uint8_t*)malloc(length);
    uint8_t * ptr = data;
    ptr[0] = this->type;
    ptr += BYTE;
    ptr[0] = length;

    for(peer * peer : *neighbors)
    {
        memcpy(ptr, peer, sizeof(*peer));
        ptr += sizeof(peer);
    }
}

void Neighbors::print(std::ostream &os) const
{
    os << "Neighbors (" << neighbors->size() << ")" << std::endl;
    for(peer * p : *neighbors)
    {
        os << "- id=0x" << std::hex << p->id << " address=";
        char address_buf[128];
        inet_ntop(AF_INET6, &p->address->sin6_addr.s6_addr, address_buf, 128);
        os << address_buf;
        os << std::dec << ":" << ntohs(p->address->sin6_port) << std::endl;
    }
}
