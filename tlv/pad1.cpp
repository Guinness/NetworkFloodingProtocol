
#include "pad1.h"

#include <cstdlib>
#include <cstring>

void Pad1::serialize(uint8_t * &data, uint16_t &length)
{
    length = 1;
    data = (uint8_t*)malloc(sizeof(uint8_t));
    memset(data, 0, 1);
}

void Pad1::print(std::ostream &os) const
{
    os << "Pad1" << std::endl;
}
