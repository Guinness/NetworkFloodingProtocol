#include "nr.h"
#include "kernel.h"

#include <arpa/inet.h>
#include <cstdlib>
#include <cstring>
#include <iostream>

NeighborRequest::NeighborRequest(uint8_t * &data, uint16_t length)
    : TLV(NEIGHBORREQUEST_T)
{
    data += BYTE; // skipping Type
    if(length < 2) {
        std::cerr << "Malformed Neighbor Request (length(" << length
            << ") < 2)" << std::endl;
#ifdef __RESILIENT__
        // If the length is not present, the neighbor request can still be
        // considered a neighbor request even though the specification states
        // it is invalid
#else
        throw DeserializationError();
#endif
    } else {
        uint8_t garbage_length = data[0];
        data += BYTE; // skipping Length
        data += garbage_length; // skipping garbage
    }
}

void NeighborRequest::serialize(uint8_t * &data, uint16_t &length)
{
    length = 2 * BYTE;
    data = (uint8_t*)malloc(length);
    uint8_t* ptr = data;
    ptr[0] = this->type;
    memset(data + BYTE, 0, BYTE);
}

void NeighborRequest::print(std::ostream &os) const
{
    os << "NeighborRequest" << std::endl;
}
