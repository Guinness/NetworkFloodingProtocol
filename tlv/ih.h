#pragma once

#include "tlv.h"

#include <cstdint>
#include <ostream>

class IHave : public TLV
{
    private:
        uint32_t seqno;
        uint64_t id;
    public:
        IHave(uint32_t seqno, uint64_t id)
            : TLV(IHAVE_T), seqno(seqno), id(id) {}
        IHave(uint8_t * &data, uint16_t length); // deserialization constructor
        void serialize(uint8_t * &data, uint16_t &length) override;
        void print(std::ostream &os) const override;
        uint64_t get_id() const { return id; }
        uint32_t get_seqno() const { return seqno; }
};
