#pragma once

#include "tlv.h"

#include <cstdint>

class PadN : public TLV
{
    private:
        uint8_t length;
    public:
        PadN(uint8_t length) : TLV(PADN_T), length(length) {}
        PadN(uint8_t * &data, uint16_t length); // deserializing constructor
        void serialize(uint8_t * &data, uint16_t &length) override;
        void print(std::ostream &os) const override;
};
