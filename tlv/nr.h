#pragma once

#include "tlv.h"

#include <cstdint>

class NeighborRequest : public TLV
{
    public:
        NeighborRequest() : TLV(NEIGHBORREQUEST_T) {}
        // Deserializing constructor
        NeighborRequest(uint8_t * &data, uint16_t length);
        void serialize(uint8_t * &data, uint16_t &length) override;
        void print(std::ostream &os) const override;
};
