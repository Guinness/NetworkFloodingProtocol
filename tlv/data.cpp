#include "data.h"
#include "kernel.h"

#include <arpa/inet.h>
#include <cstdlib>
#include <cstring>
#include <iostream>

DataTLV::DataTLV(uint8_t * &data, uint16_t length) : TLV(DATATLV_T)
{
    data += BYTE; // skipping Type
    if(length < 2) {
        std::cerr << "Malformed Data (length(" << length << ") < 2)"
            << std::endl;
        throw DeserializationError();
    } else {
        uint8_t content_length = data[0];
        uint8_t header_length = sizeof(seqno) + sizeof(id);
        if(content_length < header_length) {
            std::cerr << "Malformed Data (content_length("
                << content_length << ") < header_length("
                << header_length << ")" << std::endl;
            throw DeserializationError();
        }
        data += BYTE; // skipping Length
        this->seqno = ntohl(*(uint32_t*)data);
        data += sizeof(seqno); // skipping seqno
        this->id = be64toh(*(uint64_t*)data);
        data += sizeof(id); // skipping id
        uint8_t contents_length = content_length - header_length;
        this->data_length = contents_length;
        // TODO check return value of malloc
        this->data = data_ptr((uint8_t*)malloc(contents_length));
        memcpy(this->data.get(), data, contents_length);
        data += contents_length; // skipping data
    }
}

void DataTLV::serialize(uint8_t * &odata, uint16_t &length)
{
    length = 2 * BYTE + sizeof(seqno) + sizeof(id) +  data_length;
    odata = (uint8_t*)malloc(length);
    uint8_t * ptr = odata;
    ptr[0] = this->type;
    ptr += BYTE;
    ptr[0] = length;
    ptr += BYTE;
    *(uint32_t*)ptr = htonl(this->seqno);
    ptr += sizeof(seqno);
    *(uint64_t*)ptr = htobe64(this->id);
    ptr += sizeof(id);
    memcpy(ptr, data.get(), data_length);
}

void DataTLV::print(std::ostream &os) const
{
    // TLV Header
    os << "Data Id(" << std::hex << +id << std::dec;
    os << ") Seqno(" << +seqno << ")";
    os << " Length(" << +data_length << ")" << std::endl;
    os << std::hex;
    // Try and show data in utf8 text
    char buffer[257];
    memcpy(buffer, data.get(), data_length);
    buffer[data_length] = 0;
    os << buffer << std::endl;
    // Dump the data
    for(uint8_t i = 0; i < data_length; i++)
    {
        uint8_t value = data.get()[i];
        os << "0x" << (value < 16 ? "0" : "") << +value << " ";
        if(i % 8 == 7)
            os << std::endl;
    }
    os << std::dec << std::endl;
}
