#include <sstream>
#include <iterator>
#include <vector>
#include <iostream>
#include <ctime>
#include <string>
#include <cstring>
#include <fstream>
#include <list>
#include <arpa/inet.h>

#include "server_udp.h"
#include "peermanager.h"


#define ADDRESS_JCH "2001:660:3301:9200::51c2:1b9b"
uint64_t id_jch = 0x68fa34ac85cf9349;
uint16_t port_jch = 1212;

PeerManager * host;

void add_jch() {
    /* The default test peer from Julisz Chroboczeck */
    sockaddr_in6 address;
    inet_pton(AF_INET6, ADDRESS_JCH, address.sin6_addr.s6_addr);
    address.sin6_port = htons(port_jch);
    address.sin6_family = AF_INET6;
    peer * jch = host->add_peer(id_jch, address);
    host->move_to_potential(jch);
}

void split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
}

void add_peer(uint64_t id, const char* ipv6, uint8_t port) {
    /* Make a peer from the parsed data */
    struct sockaddr_in6 saddr;
    saddr.sin6_port = htons(port);
    saddr.sin6_family = AF_INET6;
    inet_pton(AF_INET6, ipv6, saddr.sin6_addr.s6_addr);
    peer * newpeer = host->add_peer(id, saddr);
    host->move_to_potential(newpeer);
}

void peers_from_file(char* file) {
    /* Assumes that evey line respects the pattern: id ip port */
    std::string line;
    std::ifstream infile(file);

    while(std::getline(infile, line)) {
        std::vector<std::string> elems;
        split(line, ' ', elems);
        add_peer((uint64_t)strtol(elems[0].c_str(), NULL, 16),
                 elems[1].c_str(),
                 (uint8_t)strtol(elems[2].c_str(), NULL, 10));
    }
}

int main(int argc, char *argv[]) {
    PeerList bootstrap;
    uint64_t identifier = 0x3340F9A1571FCF4A;
    host = new PeerManager(identifier, 0);
    if (argc == 1) {
        add_jch();
    } else {
        /* Parse a file and returns all the good data.*/
        std::cout << "Received a file to parse" << std::endl;
        peers_from_file(argv[1]);
    }
    server();
    return 0;
}
