#include "log.h"
#include "peermanager.h"
#include "message.h"
#include "server_udp.h"
#include "tlv/ihu.h"
#include "tlv/nr.h"

#include <iostream>
#include <list>
#include <mutex>
#include <random>
#include <string>
#include <unistd.h>
#include <exception>

extern std::mutex output_lock;

bool operator==(const in6_addr &a, const in6_addr &b)
{
    for(unsigned int i = 0; i < 16; i++)
    {
        if(a.s6_addr[i] != b.s6_addr[i])
            return false;
    }
    return true;
}


void PeerManager::repeatedly_poke_neighbors()
{
    while(true)
    {
        log("Sending poke messages");
        poke_neighbours();
        log("Sending poke IHUs");
        poke_ihu();
        sleep(30);
        log("Sending poke messages");
        poke_neighbours();
        sleep(30);
        log("Sending poke messages");
        poke_neighbours();
        sleep(30);
        if(symetric_peers.size() < 5)
        {
            log("Asking for neighbors");
            peer * p = get_random_symetric_peer();
            if(p)
            {
                msg_ptr msg = std::make_shared<Message>(id);
                msg->add_tlv(new NeighborRequest());
                send_message(msg, p);
            }
        }
    }
}

PeerManager::PeerManager(uint64_t id, int seqno)
    : id(id), seqno(seqno),
    poke_thread(&PeerManager::repeatedly_poke_neighbors, this)
{
}

PeerManager::~PeerManager()
{
    // Freeing all peers
    for(auto pair : peermap)
        delete pair.second;
}

peer * PeerManager::add_peer(uint64_t id, sockaddr_in6 &saddr)
{
    address ad = address(saddr.sin6_addr, saddr.sin6_port);
    auto found = peermap.find(ad);
    if(found != peermap.end()) {
        return found->second;
    } else {
        peer * p = new peer(id, saddr);
        peermap.insert({ad,p});
        msg_ptr msg = std::make_shared<Message>(id);
        msg->add_tlv(new IHeardYou(p->id));
        send_message(msg, p);
        return p;
    }
}

peer * PeerManager::get_random_symetric_peer()
{
    PeerEntry * peer = symetric_peers.get_random_item();
    if(peer)
        return peer->p;
    else
        return (struct peer*)NULL;
}

void PeerManager::move_to_potential(peer * peer)
{
    if(!potential_peers.contains(peer))
    {
        potential_lock.lock();
        potential_peers.push_front(peer);
        potential_lock.unlock();
    }
    if(symetric_peers.contains(peer))
    {
        symetric_lock.lock();
        symetric_peers.remove(peer);
        symetric_lock.unlock();
    }
    if(unidirectional_peers.contains(peer))
    {
        unidirectional_lock.lock();
        unidirectional_peers.remove(peer);
        unidirectional_lock.unlock();
    }
}

void PeerManager::move_to_unidirectionnal(peer * peer) {
    if(potential_peers.contains(peer))
    {
        potential_lock.lock();
        potential_peers.remove(peer);
        potential_lock.unlock();
    }
    if(symetric_peers.contains(peer))
    {
        symetric_lock.lock();
        symetric_peers.remove(peer);
        symetric_lock.unlock();
    }
    if(!unidirectional_peers.contains(peer))
    {
        unidirectional_lock.lock();
        unidirectional_peers.push_front(peer);
        unidirectional_lock.unlock();
    }
}

void PeerManager::move_to_symetric(peer * peer) {
    if(potential_peers.contains(peer))
    {
        potential_lock.lock();
        potential_peers.remove(peer);
        potential_lock.unlock();
    }
    if(!symetric_peers.contains(peer))
    {
        symetric_lock.lock();
        symetric_peers.push_front(peer);
        symetric_lock.unlock();
    }
    if(unidirectional_peers.contains(peer))
    {
        unidirectional_lock.lock();
        unidirectional_peers.remove(peer);
        unidirectional_lock.unlock();
    }
}

void PeerManager::update_neighbours() {
    time_t ref_time = time(NULL);
    if (ref_time == (time_t)(-1)) {
        perror("Couldn't get time. Killing program");
        exit(1);
    }
    unidirectional_lock.lock();
    for (auto item:unidirectional_peers) {
        if(item->timer - ref_time < -100 ||
            item->ihu_timer - ref_time < -300) {
            unidirectional_peers.remove(item);
        }
    }
    unidirectional_lock.unlock();
    symetric_lock.lock();
    for (auto item:symetric_peers) {
        if (item->timer - ref_time < -100 ||
            item->ihu_timer - ref_time < -300) {
            symetric_peers.remove(item);
        }
    }
    symetric_lock.lock();
}

void PeerManager::update_timer(peer * peer)
{
    PeerEntry * p = symetric_peers.get_peer(peer);
    if(!p)
        p = unidirectional_peers.get_peer(peer);
    if(p) {
        p->update_timer();
    } else {
        move_to_unidirectionnal(peer);
        unidirectional_peers.get_peer(peer)->update_timer();
    }
}

void PeerManager::update_ihu(peer * peer)
{
    PeerEntry * p = symetric_peers.get_peer(peer);
    if(p) {
        p->update_ihu_timer();
    } else {
        move_to_symetric(peer);
        symetric_peers.get_peer(peer)->update_ihu_timer();
    }
}

void PeerManager::add_data_entry(data_entry & new_entry) {
    /* Adds a new peer to the list */
    DataEntry * dentry = entry_list.find_by_id(new_entry.id);
    if(dentry)
    {
        data_entry entry = dentry->get_entry();
        if(new_entry.seqno > entry.seqno)
        {
            entry.seqno  = new_entry.seqno;
            entry.data   = new_entry.data;
            entry.length = new_entry.length;
            dentry->flood();
        }
    }
    else
    {
        time_t first_time = time(NULL);
        DataEntry * value = new DataEntry(first_time, new_entry);
        entry_list.push_front(value);
        value->flood();
    }
}

void PeerManager::poke_neighbours() {
    /* Sends empty messages to all unidirectional peers and symetrical peers */
    msg_ptr msg = std::make_shared<Message>(this->id);
    unidirectional_lock.lock();
    for (auto peer:unidirectional_peers)
        send_message(msg, peer->p);
    unidirectional_lock.unlock();
    symetric_lock.lock();
    for (auto peer:symetric_peers)
        send_message(msg, peer->p);
    if (symetric_peers.size() <= 5) {
        potential_lock.lock();
        PeerEntry * chosen_one = potential_peers.get_random_item();
        potential_lock.unlock();
        if(chosen_one)
            send_message(msg, chosen_one->p);
    }
    symetric_lock.unlock();
}

void PeerManager::poke_ihu() {
    /* Sends a IHU-TLV to all unidirectional_peers and symetrical peers */
    unidirectional_lock.lock();
    for (auto peer:unidirectional_peers) {
        msg_ptr message = std::make_shared<Message>(id);
        message->add_tlv(new IHeardYou(peer->p->id));
        send_message(message, peer->p);
	}
    unidirectional_lock.unlock();
    symetric_lock.lock();
    for (auto peer:symetric_peers) {
        msg_ptr message = std::make_shared<Message>(id);
        message->add_tlv(new IHeardYou(peer->p->id));
        send_message(message, peer->p);
	}
    symetric_lock.unlock();
}

void PeerManager::poke_bidirectionnal() {
    /* Choose randomly a peer in symetrical peers, and sends a NeighborRequest
     * TLV */
    symetric_lock.lock();
    PeerEntry * peer = symetric_peers.get_random_item();
    symetric_lock.unlock();
    if(!peer)
        return;
    msg_ptr message = std::make_shared<Message>(id);
    message->add_tlv(new NeighborRequest());
    send_message(message, peer->p);
}

void PeerManager::peer_has_data(uint64_t data_id, peer * peer)
{
    entry_list.find_by_id(data_id)->peer_has_it(peer);
}

void PeerManager::delete_obsolete_data(time_t reftime) {
    /* 2100 is 35 minutes converted in seconds */
    entry_list.remove_old(2100, reftime);
}

PeerList & PeerManager::construct_flood() {
    /* Returns the list initializing the flooding state */
    return symetric_peers;
}
