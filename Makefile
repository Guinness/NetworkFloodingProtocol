SRCH = $(wildcard *.cpp)
OBJ = $(subst .cpp,.o,$(SRC))
TLV = $(wildcard tlv/*.cpp)
SRC = $(SRCH) $(TLV)

default: $(OBJ) $(UTILS)
	g++ -g -pedantic -Wall -Wextra -std=c++11 $(OBJ) -o flood -lpthread
	gcc -g -pedantic -Wall -std=c99 test_udp.c -o test_udp

%.o: %.cpp
	g++ -c -g -pedantic -Wall -Wextra -std=c++11 $< -o $@ -lpthread

clean:
	@rm *.o flood test_udp tlv/*.o
