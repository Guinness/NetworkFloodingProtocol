#include "message.h"

#include <arpa/inet.h>
#include <cstring>
#include <iostream>
#include <list>
#include <string>
#include <tuple>

#if defined(__linux__)
#  include <endian.h>
#elif defined(__FreeBSD__) || defined(__NetBSD__)
#  include <sys/endian.h>
#elif defined(__OpenBSD__)
#  include <sys/types.h>
#  define be16toh(x) betoh16(x)
#  define be32toh(x) betoh32(x)
#  define be64toh(x) betoh64(x)
#endif


void Message::deserialize_body(uint8_t * data, uint32_t length)
{
    while(length > 0)
    {
        uint8_t * last = data;
        TLV * tlv = deserialize(data, length);
        if(!tlv)
            throw DeserializationError();
        body.push_back(tlv);
        uint16_t size = data - last; // data is moved by the length of the tlv
        if(size == 0) // Empty tlv ? That makes no sense
            throw DeserializationError();
        length -= size;
    }
}

Message::Message (uint64_t id) : version(0), id(id) {}

Message::Message(uint8_t * data, uint32_t length)
{
    if(length < MSG_HEADER_SIZE) {
        std::cerr << "Message too short" << std::endl;
        throw DeserializationError();
    }
    if(*data != MSG_MAGIC) {
        std::cerr << "Wrong magic value" << std::endl;
        throw DeserializationError();
    }
    data += BYTE; // skipping Magic
    this->version = *data;
    if(this->version != 0) {
        std::cerr << "Wrong version number (" << this->version << ")"
                  << std::endl;
        throw DeserializationError();
    }
    data += BYTE; // skipping version
    uint16_t body_length = ntohs(*(uint16_t*)data);
    data += sizeof(body_length); // skipping Body length
    this->id = be64toh(*(uint64_t*)data);
    data += sizeof(this->id); // skipping Id
    deserialize_body(data, body_length);
}

Message::~Message()
{
    for(TLV * tlv : body)
        delete tlv;
}

void Message::add_tlv(TLV* tlv)
{
    body.push_back(tlv);
}

void Message::serialize(uint8_t * &data, uint32_t &length)
{
    typedef std::pair<uint8_t*,uint16_t> raw_data;
    std::list<raw_data> serialized;
    uint16_t body_length = 0;
    for(TLV * tlv : body)
    {
        uint8_t * tlv_data;
        uint16_t  tlv_length;
        tlv->serialize(tlv_data, tlv_length);
        body_length += tlv_length;
        serialized.push_back(raw_data(tlv_data,tlv_length));
    }
    length = MSG_HEADER_SIZE + body_length;
    data = (uint8_t*)malloc(length);
    data[0] = 57; data[1] = this->version;
    *(uint16_t*)(data + 2) = htons(body_length);
    *(uint64_t*)(data + 4) = htobe64(this->id);
    uint8_t * tlv_base = data + MSG_HEADER_SIZE;
    for(raw_data tlv : serialized)
    {
        uint8_t * tlv_data = std::get<0>(tlv);
        uint16_t  tlv_length = std::get<1>(tlv);
        memcpy(tlv_base, tlv_data, tlv_length);
        tlv_base += tlv_length;
        free(tlv_data);
    }
}

std::ostream & operator<<(std::ostream &os, const Message& msg)
{
    os << "Message (protocol version " << (unsigned int)msg.version << ")" << std::endl;
    os << "Id = 0x" << std::hex << msg.id << std::dec << std::endl;
    for(TLV * tlv : msg.body)
    {
        tlv->print(os);
    }
    return os;
}
