#pragma once

#include <cstdint>
#include <list>
#include <ostream>
#include <memory>

#include "tlv/tlv.h"

#define MSG_MAGIC 57
#define MSG_HEADER_SIZE 12

class Message {
    private:
        uint8_t version;
        uint64_t id;
        std::list<TLV*> body;
        // deserializes a list of TLVs into body
        void deserialize_body(uint8_t * data, uint32_t length);
    public:
        Message(uint64_t id);
        Message(uint8_t * data, uint32_t length); // deserializing constructor
        ~Message();
        uint8_t get_version() { return version; }
        /* /!\ Pointers are owned by the message instance, they are
         * deleted on message destruction */
        void add_tlv(TLV* tlv);
        void serialize(uint8_t * &data, uint32_t &length);
        uint64_t get_id() { return id; }
        std::list<TLV*> & get_tlvs() { return body; }
        friend std::ostream & operator<<(std::ostream &os, const Message& msg);
};

typedef std::shared_ptr<Message> msg_ptr;
