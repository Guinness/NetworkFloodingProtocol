#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <cstring>
#include <csignal>
#include <vector>
#include <iostream>
#include <string>
#include <queue>
#include <thread>
#include <mutex>

#include "log.h"
#include "peermanager.h"
#include "server_udp.h"
#include "utils.h"
#include "tlv/headers.h"

#define PORT 1212
#define BUFFER_SIZE 8192


using namespace std;

extern PeerManager * host;
int sock;
extern std::mutex output_lock;


/*----------------------------------------------------------------------------*/
/*                             Signal Handling                                */
/*----------------------------------------------------------------------------*/

void sigint_handler(int signal) {
    /* Handles SIGINT in order to close all sockets */
    cout << endl << "Received signal SIGINT (" << signal << ")" << endl;
    /* Closing main socket */
    cout << "Closing socket " << sock << endl;
    close(sock);
    exit(signal);
}


/*----------------------------------------------------------------------------*/
/*                           Managing sockets                                 */
/*----------------------------------------------------------------------------*/

int create_socket() {
    int sock;

    if ((sock  = socket(PF_INET6, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("Error in socket creation");
        exit(1);
    }
    int val = 1;
    int error;

    if ((error = setsockopt(
                    sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val))
        ) < 0) {
        perror("Error in socket modification");
        exit(2);
    }
    int morph = 0;
    if ((error = setsockopt(
                    sock, IPPROTO_IPV6, IPV6_V6ONLY, &morph, sizeof(morph))
        ) < 0) {
        perror("Error in socket modification");
        exit(3);
    }
    return sock;
}

void bind_to_sock(int sock, struct sockaddr_in6 &sin6){
    if (bind(sock,
            (struct sockaddr *) &sin6,
            sizeof(sin6)) < 0 ) {
        perror("Error in binding socket");
    }
}


/*----------------------------------------------------------------------------*/
/*                           Managing peers                                   */
/*----------------------------------------------------------------------------*/

typedef std::pair<msg_ptr,peer*> packet;

std::queue<packet> packets;
std::mutex packets_lock;

/** Queues the message and peer for delivery */
void send_message(msg_ptr msg, peer * peer)
{
    packets_lock.lock();
    packets.push(packet(msg,peer));
    packets_lock.unlock();
}

/** Sends the message msg to the given peer over UDP */
void send_packet(packet packet)
{
    msg_ptr msg = std::get<0>(packet);
    peer * peer = std::get<1>(packet);
    output_lock.lock();
    std::cout << std::endl;
    std::cout << "--- SENDING ---" << std::endl
              << *peer
              << "---------------" << std::endl
              << *msg
              << "---------------" << std::endl << std::endl;
    output_lock.unlock();
    uint8_t * msg_data;
    uint32_t msg_len;
    msg->serialize(msg_data, msg_len);
    int err = sendto(sock,
                     msg_data, msg_len,
                     0,
                     (const struct sockaddr*)peer->address,
                     sizeof(*(peer->address)));
    if(err < 0)
    {
        perror("Could not send message");
        memdump(msg_data, msg_len);
    }
    free(msg_data);
}

/** Sends all queued packets */
void send_packets()
{
    packets_lock.lock();
    while(!packets.empty())
    {
        send_packet(packets.front());
        packets.pop();
    }
    packets_lock.unlock();
}

/** Sends all queued packets every half a second */
void repeatedly_send_packets()
{
    while(true)
    {
        send_packets();
        usleep(500000);
    }
}

/** Handles a TLV. peer is the peer that sent the tlv */
void handle_tlv(TLV * tlv, peer * peer)
{
    switch(tlv->get_type())
    {
        case IHEARDYOU_T:
            host->update_ihu(peer);
            host->update_timer(peer);
            break;
        case NEIGHBORREQUEST_T:
            {
                msg_ptr msg = std::make_shared<Message>(host->get_id());
                list_ptr<struct peer*> neighbors =
                    std::make_shared<std::list<struct peer*>>();
                for(unsigned int i = 0; i < 5; i++) {
                    struct peer * phost = host->get_random_symetric_peer();
                    if(phost && phost != peer)
                        neighbors->push_front(phost);
                }
                msg->add_tlv(new Neighbors(neighbors));
                send_message(msg, peer);
                break;
            }
        case DATATLV_T:
            {
                DataTLV * t = (DataTLV*)tlv;
                data_entry entry{t->get_id(),
                                 t->get_seqno(),
                                 t->get_data(),
                                 t->get_data_length()};
                host->add_data_entry(entry);
                break;
            }
        case IHAVE_T:
            {
                IHave * t = (IHave*)tlv;
                host->peer_has_data(t->get_id(), peer);
                break;
            }
        default:
            std::cerr << "TLV reaction not implemented (" << +tlv->get_type()
                      << ")" << std::endl;
            tlv->print(std::cerr);
            break;
    }
}

/** Handles a message. peer is the peer that sent the message */
void handle_message(Message &msg, peer * peer)
{
    for(TLV * tlv : msg.get_tlvs())
        handle_tlv(tlv, peer);
}

/** Waits for an incoming message and handles it */
void wait_message()
{
    uint8_t buf[BUFFER_SIZE];
    struct sockaddr_in6 sin6_other;
    unsigned int slen = sizeof(sin6_other);
    int data_length = recvfrom(sock,
                               buf, BUFFER_SIZE,
                               0,
                               (struct sockaddr*)&sin6_other, &slen);
    if(data_length < 0) {
        perror("Error receiving data from client");
    } else {
        /* Accepts new peer, adds it to the vector */
        try
        {
            Message msg(buf, data_length);
            peer * p = host->add_peer(msg.get_id(), sin6_other);
            output_lock.lock();
            std::cout << std::endl;
            std::cout << "--- RECEIVING ---" << std::endl;
            std::cout << *p;
            std::cout << "-----------------" << std::endl;
            std::cout << msg;
            std::cout << "-----------------" << std::endl << std::endl;
            output_lock.unlock();
            host->update_timer(p);
            handle_message(msg, p);
        }
        catch(DeserializationError &e)
        {
            output_lock.lock();
            std::cout << "Deserialization error" << std::endl;
            memdump(buf, data_length);
            output_lock.unlock();
        }
    }
}

/** Starts the server */
int server() {
    sock = create_socket();
    struct sockaddr_in6 sin6;
    memset(&sin6, 0, sizeof(sin6));
    sin6.sin6_port = htons(PORT);
    sin6.sin6_family =  AF_INET6;
    if (bind(sock,
             (struct sockaddr*) &sin6,
             sizeof(sin6)) < 0) {
        perror("Error in binding socket");
        exit(3);
    }
    std::thread sender(repeatedly_send_packets);
    signal(SIGINT, sigint_handler);
    while(1) {
        wait_message();
    }
    delete host;
    close(sock);
    return 0;
}
