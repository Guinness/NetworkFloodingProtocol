#include "log.h"

#include <mutex>
#include <iostream>

/** Lock on the output. A bit inconvenient when using cout */
std::mutex output_lock;

void log(std::string msg)
{
    output_lock.lock();
    std::cout << msg << std::endl;
    output_lock.unlock();
}
