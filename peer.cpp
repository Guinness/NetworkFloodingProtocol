#include "peer.h"

#include <arpa/inet.h>

#include <cstring>

peer::peer(uint64_t id, sockaddr_in6 &address)
    : id(id)
{
    this->address = (sockaddr_in6*)malloc(sizeof(sockaddr_in6));
    memcpy(this->address, &address, sizeof(sockaddr_in6));
}

std::ostream & operator<<(std::ostream &os, const peer& p)
{
    char address[128];
    inet_ntop(AF_INET6, &p.address->sin6_addr, address, 128);
    os << std::dec;
    os << address << std::endl << "Port " << ntohs(p.address->sin6_port)
       << std::endl << "Id " << std::hex << p.id << std::endl;
    return os;
}
