#pragma once

#include <cstdint>
#include <ctime>
#include <list>
#include <memory>
#include <mutex>
#include <thread>

#include "peer.h"
#include "peerlist.h"

typedef std::shared_ptr<uint8_t> data_ptr;

/* The struct entry in the data list. It contains the id of the peer, the seqno of the
 * data, the data, and the length of this data. */
struct data_entry {
    uint64_t id;
    uint32_t seqno;
    data_ptr data;
    uint8_t length;
};

/* An entry for the data. It contains the first time a version has been seen */
class DataEntry {
    private:
        time_t first_time;
        data_entry entry;

        std::thread flood_thread;
        std::mutex floodlist_lock;
        PeerList floodlist;
        void flood_protocol();
    public:
        uint64_t get_id();
        DataEntry(time_t, data_entry);
        virtual ~DataEntry() {}
        time_t get_time();

        // Entry getter
        data_entry & get_entry() { return entry; }

        // Floods the data
        void flood();
        // A peer has sent a IHave TLV for this data
        void peer_has_it(peer * peer);
};

class DataList : public std::list<DataEntry*> {
    public:
        // Adds a new entry to the list or updates an existing one
        void add_data(data_entry &entry);
        /* Finds a data entry by id */
        DataEntry * find_by_id(uint64_t id);
        void remove_old(int, time_t);
};
