#pragma once

#include <list>

#include "peerentry.h"

class PeerList : public std::list<PeerEntry*> {
    public:
        PeerList() : std::list<PeerEntry*>() {} // Default constructor
        PeerList(const PeerList & lst); // Copy constructor
        /* Gets a peer entry corresponding to peer, NULL if nonexistent */
        PeerEntry * get_peer(peer * p);
        /* Returns whether there is a peer entry for p */
        bool contains(peer * p);
        /* Gets uniformly a random item of the list. Useful for maintaining the
         * neighbor list (4.2 in the pdf) */
        PeerEntry * get_random_item();
        /* Gets uniformly 'int' items of the list. Idem, for 4.2 section */
        PeerList get_random_items(unsigned int);
        /* removes the peer identified by the structure from the list */
        void remove(peer * peer);
        /* Removes the PeerEntry from the list */
        void remove(PeerEntry * entry);
        /* Push front the peer identified by the struct peer in the list */
        void push_front(peer * peer);
        /* Idem with a peer entry (for example when moving from one list to
         * another. */
        void push_front(PeerEntry * entry);
        /* Random access */
        PeerEntry * operator[](unsigned int i) const;
};
