#pragma once

#include <cstdint>
#include <ctime>
#include <list>
#include <netinet/in.h>
#include <netdb.h>
#include <mutex>
#include <tuple>
#include <string>
#include <sys/socket.h>
#include <thread>
#include <unordered_map>

#include "peer.h"
#include "peerentry.h"
#include "peerlist.h"
#include "datalist.h"

bool operator==(const in6_addr &a, const in6_addr &b);

class PeerManager {
    private:
       uint64_t id;
       int seqno;

       typedef std::pair<in6_addr,in_port_t> address;
       /* Defining a hash function for an address:port pair */
       struct address_hash {
           std::size_t operator()(const address &a) const {
               std::size_t sum = 0;
               for(unsigned int i = 0; i < 16; i++)
                   sum += a.first.s6_addr[i];
               return sum + a.second;
           }
       };
       /* A map from address:port to peers */
       std::unordered_map<address,peer*,address_hash> peermap;

       DataList entry_list;
       PeerList potential_peers;
       PeerList unidirectional_peers;
       PeerList symetric_peers;

       /* Locks on the lists */
       std::mutex potential_lock;
       std::mutex unidirectional_lock;
       std::mutex symetric_lock;

       /* Threads managing poking and the likes */
       std::thread poke_thread;

       /* Sends empty messages to all symmetrical and unidirectional peers */
       void poke_neighbours();
       /* Sends ihu to all unidirectionnal and symmetric peers..
        * int is a sock, but I don't know if it is a good idea ? */
       void poke_ihu();
       /* Randomly choose a bidirectionnal peer, and sends a NeighborRequest TLV to this peer.
        * int is the socket to use */
       void poke_bidirectionnal();
       /* Constantly pokes neighbors every 30/90 seconds */
       void repeatedly_poke_neighbors();
    public:
       PeerManager(uint64_t id, int seqno);
       ~PeerManager();
       /* Adds a peer */
       peer * add_peer(uint64_t id, sockaddr_in6 &saddr);
       /* Get a random peer */
       peer * get_random_symetric_peer();
       /* Moves the peer to potential peer list */
       void move_to_potential(peer * peer);
       /* Moves the peer to unidirectionnal_peer list */
       void move_to_unidirectionnal(peer * peer);
       /* Moves the peer to symetric_peer list */
       void move_to_symetric(peer * peer);
       /* Update the neighbours. Checks all the lists according to the first
        * paragraph of 6th page */
       void update_neighbours();
       /* Resets the message timer of the peer */
       void update_timer(peer * peer);
       /* Resets the ihu timer of the peer */
       void update_ihu(peer * peer);
       /* Adds a data entry to the list of Data (entry_list in the prototype) */
       void add_data_entry(data_entry & data);
       /* Propagates IHave TLV to data */
       void peer_has_data(uint64_t data_id, peer * peer);
       /* Delete all obsolete data, aka data older than 35 minutes*/
       void delete_obsolete_data(time_t);
       /* Constructs the initial list for the flooding protocol. */
       PeerList & construct_flood();
       /* Getters */
       uint64_t get_id() { return id; }
};

