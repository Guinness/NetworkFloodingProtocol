#include "peerentry.h"

#include <cstdio>

PeerEntry::PeerEntry(peer * peer)
    : p(peer), timer(time(NULL)), ihu_timer((time_t)(-1))
{
}

void PeerEntry::update_timer() {
    time_t result, cur_time;
    result =  time(&cur_time);
    if (result == (time_t)(-1)){
        perror("Couldn't get time. Skipping");
        return;
    }
    timer = cur_time;
}

void PeerEntry::update_ihu_timer() {
    time_t result, cur_time;
    result =  time(&cur_time);
    if (result == (time_t)(-1)){
        perror("Couldn't get time. Skipping");
        return;
    }
    ihu_timer = cur_time;
}

