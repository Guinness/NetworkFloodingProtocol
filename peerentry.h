#pragma once

#include <time.h>

#include "peer.h"

/* This is an entry in a list of peer of the Peer class. The struct peer is the
 * peer itself, timer is the last time a packet has been received, and
 * ihu_timer the last time we received and ihu tlv.  */
class PeerEntry {
    public:
        peer * p;
        time_t timer;
        time_t ihu_timer;
        PeerEntry(peer * peer);

        bool operator== (const PeerEntry other) {
            return (this->p == other.p);
        }
        bool operator== (peer * peer) {
            return (this->p == peer);
        }
        void set_timer(time_t value) { timer = value; }
        void update_timer(); // Updates timer
        void update_ihu_timer(); // Updates ihu_timer
};
