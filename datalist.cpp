#include <unistd.h>

#include "datalist.h"
#include "peermanager.h"
#include "message.h"
#include "server_udp.h"
#include "tlv/headers.h"

extern PeerManager * host;

void send_to_peerlist(msg_ptr &msg, PeerList &peerlist)
{
    for(PeerEntry * peer : peerlist)
        send_message(msg, peer->p);
}

void DataEntry::flood_protocol()
{
    msg_ptr msg = std::make_shared<Message>(host->get_id());
    msg->add_tlv(new DataTLV(entry.seqno,
                             entry.id,
                             entry.data,
                             entry.length));
    floodlist_lock.lock();
    floodlist = PeerList(host->construct_flood());
    floodlist_lock.unlock();
    unsigned int wave = 4;
    do {
        floodlist_lock.lock();
        if(floodlist.size() == 0)
            return;
        send_to_peerlist(msg, floodlist);
        floodlist_lock.unlock();
        sleep(3);
        wave--;
    } while(wave > 0);
    floodlist_lock.lock();
    if(floodlist.size() == 0)
        return;
    floodlist_lock.unlock();
    // Remove floodlist from symetric_peers
}

void DataEntry::peer_has_it(peer * peer)
{
    floodlist_lock.lock();
    floodlist.remove(peer);
    floodlist_lock.unlock();
}

DataEntry::DataEntry(time_t timer, data_entry new_entry)
    : first_time(timer), entry(new_entry)
{
}

uint64_t DataEntry::get_id() {
    return(entry.id);
}

time_t DataEntry::get_time() {
    return(first_time);
}

void DataEntry::flood()
{
    if(!flood_thread.joinable())
        flood_thread = std::thread(&DataEntry::flood_protocol, this);
}

DataEntry* DataList::find_by_id(uint64_t identifier) {
    try {
        for(auto item:*this) {
            if (item->get_id() == identifier) {
                return(item);
            }
        }
        throw 20;
    } catch (int e) {
        return((DataEntry*)NULL);
    }
}


void DataList::remove_old(int delta, time_t reftime) {
    for (auto data:*this) {
        if (reftime - data->get_time() > delta) {
            this->remove(data);
        }
    }
}

