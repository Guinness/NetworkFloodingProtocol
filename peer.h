#pragma once

#include <cstdint>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>

struct peer
{
    uint64_t id;
    sockaddr_in6* address;

    peer() {}
    peer(uint64_t id, sockaddr_in6 &address);

    bool operator== (const struct peer peer) {
        return (id == peer.id && address == peer.address);
    }
    friend std::ostream & operator<<(std::ostream &os, const peer& p);
};
