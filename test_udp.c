#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<arpa/inet.h>

#define PORT 1212

int main() {
    int sock;
    if((sock = socket(PF_INET6, SOCK_DGRAM, 0)) < 0) {
        perror("Error creating socket");
        exit(1);
    }
    struct in6_addr sin6;
    inet_pton(AF_INET6, "::1", sin6.s6_addr);
    struct sockaddr_in6 saddr;
    saddr.sin6_port = htons(PORT);
    saddr.sin6_family = AF_INET6;
    saddr.sin6_addr = sin6;

    unsigned char* request[48];
    memcpy(&request,"This is an example of request.\n", 45);
    int err;
    if ((err = sendto(
                    sock,
                    (void*)request,
                    sizeof(request),
                    0,
                    (const struct sockaddr*)&saddr,
                    sizeof(saddr))) < 0) {
        perror("Error sending message");
    }
    close(sock);
    return(0);
}
