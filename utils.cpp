#include "utils.h"

#include <iomanip>
#include <iostream>
#include <mutex>

extern std::mutex output_lock;

void memdump(uint8_t * data, unsigned int length)
{
    output_lock.lock();
    std::cout << std::hex;
    for(unsigned int i = 0; i < length; i++)
    {
        std::cout << "0x" << (data[i] < 16 ? "0" : "") << +data[i] << " ";
        if(i % 8 == 7)
            std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << std::dec;
    output_lock.unlock();
}
