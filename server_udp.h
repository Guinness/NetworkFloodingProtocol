#pragma once

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <ctime>

#include "message.h"
#include "peer.h"

class PeerList;

int create_socket();
void bind_to_sock();
int server();
void send_message(msg_ptr msg, peer * peer);
