#include "peerlist.h"

#include <iostream>
#include <mutex>
#include <random>

extern std::mutex output_lock;

PeerList::PeerList(const PeerList & lst) : std::list<PeerEntry*>()
{
    for(PeerEntry * peer : lst)
        push_front(peer);
}

PeerEntry * PeerList::get_peer(peer * p)
{
    for(PeerEntry * peer : *this)
    {
        if(peer->p == p)
            return peer;
    }
    return (PeerEntry*)NULL;
}

bool PeerList::contains(peer * p)
{
    return (bool)get_peer(p);
}

void PeerList::remove(peer * peer) {
    for(PeerEntry * item : *this) {
        if(item->p == peer) {
            std::list<PeerEntry*>::remove(item);
            return;
        }
    }
}

void PeerList::remove(PeerEntry * entry) {
    for (auto item:*this) {
        if (item == entry) {
            std::list<PeerEntry*>::remove(entry);
            break;
        }
    }
}

void PeerList::push_front(peer * peer) {
    PeerEntry * peer_en = new PeerEntry(peer);
    peer_en->update_timer();
    std::list<PeerEntry*>::push_front(peer_en);
}

void PeerList::push_front(PeerEntry * entry) {
    entry->update_timer();
    std::list<PeerEntry*>::push_front(entry);
}

PeerEntry * PeerList::get_random_item() {
    if(size() == 0)
        return (PeerEntry*)NULL;
    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(0,size()-1);
    int roll = distribution(generator);
    PeerEntry * res = (*this)[roll];
    return res;
}

PeerList PeerList::get_random_items(unsigned int number) {
    unsigned int size = this->size();
    PeerList res = {};
    if (size < number) {
        perror("Less items in list than requested numbers");
        res = *this;
    } else {
        std::default_random_engine generator;
        std::uniform_int_distribution<int> distribution(1,size);
        std::list<int> chosen_items = {};
        while (chosen_items.size() != number) {
            int roll = distribution(generator);
            int absent = 1;
            for (auto index:chosen_items) {
                if (index == roll) {
                    absent = 0;
                }
            }
            if (absent) { chosen_items.push_front(roll);}
        }
        chosen_items.sort();
        for (auto index:chosen_items) {
            PeerEntry * new_entry = (*this)[index];
            res.push_front(new_entry);
        }
    }
    return(res);
}

PeerEntry * PeerList::operator[](unsigned int i) const
{
    if(size() > i)
    {
        auto it = std::next(begin(), i);
        return *it;
    }
    return (PeerEntry*)NULL;
}
