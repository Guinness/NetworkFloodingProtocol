#pragma once

#include <string>

/** Thread-safe logging function */
void log(std::string msg);
